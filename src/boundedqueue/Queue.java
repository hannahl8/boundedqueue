package boundedqueue;

/**
 * <p>
 * A bounded, first-in-first-out (FIFO) data structure. Elements in the queue may not be null.
 * </p>
 * <p>
 * A typical string representation of a bounded queue is:
 * <code>[E_1, E_2, ..., E_n-1, E_n]:c</code>
 * where <code>E_1</code> is the front element of the queue
 * and <code>E_n</code> is the end element of the queue
 * and <code>c</code> is the capacity of the queue
 * </p>
 *
 * <p>
 * Implementations of this interface should have a one-argument constructor that takes
 * the desired capacity and creates a new, empty queue with that capacity.
 * The capacity must be strictly greater than zero.
 * </p>
 *
 * <p><code>public Queue(int capacity)</code></p>
 *
 * <p>A queue iterator iterates through the queue from bottom to top.</p>
 *
 * @param <E> the type of elements in the queue
 * @author Hannah Lyons (hannahl8)
 */
public interface Queue<E> extends Iterable<E> {
    /**
     * <p>Add an element to the end of the queue.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> <em>and</em> <code>x = X</code> }<br>
     * <code>q.enqueue(x)</code><br>
     * { <code>q = [A, B, C, X]:6</code> }<br>
     * </p>
     *
     * @param element E the element to enqueued to the end of the queue
     * @throws IllegalStateException    if the queue is full
     * @throws IllegalArgumentException if the element is null
     */
    void enqueue(E element) throws IllegalStateException, IllegalArgumentException;

    /**
     * <p>Remove and return the element at the front of the queue.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> }<br>
     * <code>q.dequeue()</code><br>
     * { <code>q = [B, C]:6</code> <em>and</em> <code>return = A</code> }<br>
     * </p>
     *
     * @return E the element at the front of the queue
     * @throws IllegalStateException if the queue is empty
     */
    E dequeue() throws IllegalStateException;

    /**
     * <p>Returns the number of elements in the queue.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> }<br>
     * <code>q.length()</code><br>
     * <code>return = 3</code><br>
     * </p>
     *
     * @return int the number of elements in the queue
     */
    int length();

    /**
     * <p>Returns the maximum number of elements the queue can hold.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> }<br>
     * <code>q.capacity()</code><br>
     * <code>return = 6</code><br>
     * </p>
     *
     * @return int the maximum number of elements the queue can hold
     */
    int capacity();


    /**
     * <p>Returns a new, empty bounded queue with the same capacity as the calling queue.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> }<br>
     * <code>q.newInstance()</code><br>
     * <code>return = []:6</code><br>
     * </p>
     *
     * @return Queue a new, empty bounded queue with the same capacity as the calling queue
     */
    Queue<E> newInstance();

    /**
     * <p>Removes all elements from the queue.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> }<br>
     * <code>q.clear()</code><br>
     * { <code>q = []:6</code> }<br>
     * </p>
     */
    void clear();

    /**
     * <p>Checks if the queue has no elements.</p>
     * <p>
     * Example:<br>
     * { <code>q = []:6</code> }<br>
     * <code>q.isEmpty()</code><br>
     * <code>return = true</code><br>
     * </p>
     *
     * @return boolean true if the queue has no elements, false otherwise
     */
    boolean isEmpty();

    /**
     * <p>Checks if the queue capacity is full.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C, D, E, F]:6</code> }<br>
     * <code>q.isFull()</code><br>
     * <code>return = true</code><br>
     * </p>
     *
     * @return boolean true if the queue capacity is full, false otherwise
     */
    boolean isFull();

    /**
     * <p>Appends all elements from the given queue to the end of the calling queue.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> <em>and</em> <code>that = [D, E, F]:6</code> }<br>
     * <code>q.appendAll(that)</code><br>
     * { <code>q = [A, B, C, D, E, F]:6</code> <em>and</em> <code>that = [D, E, F]:6</code> }<br>
     * </p>
     *
     * @param that Queue the queue to append to the end of the calling queue.
     * @throws IllegalStateException if the calling queue does not have enough capacity to append all elements from the given queue
     * @throws IllegalArgumentException if the given queue is null
     */
    void appendAll(Queue<E> that) throws IllegalStateException, IllegalArgumentException;

    /**
     * <p>Returns a new queue with the same elements as the calling queue.</p>
     * <p>
     * Example:<br>
     * { <code>q = [A, B, C]:6</code> }<br>
     * <code>q.copy()</code><br>
     * <code>return = [A, B, C]:6</code><br>
     * </p>
     *
     * @return Queue a new queue with the same elements as the calling queue
     */
    Queue<E> copy();
}
