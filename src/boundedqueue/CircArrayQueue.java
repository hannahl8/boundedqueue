package boundedqueue;

import java.util.Iterator;

public class CircArrayQueue<E> extends AbstractQueue<E> {

    private E[] elements;
    private int first;
    private int last;
    private int length;

    public CircArrayQueue(int capacity) {
        super(capacity);
        this.elements = (E[]) new Object[capacity];
        this.first = 0;
        this.last = 0;
        this.length = 0;
    }

    @Override
    public void enqueue(E element) throws IllegalStateException, IllegalArgumentException {
        if (isFull()) {
            throw new IllegalStateException("Queue is full");
        }
        if (element == null) {
            throw new IllegalArgumentException("Element cannot be null");
        }

        elements[last] = element;
        last = (last + 1) % capacity();
        length++;
    }

    @Override
    public E dequeue() throws IllegalStateException {
        if (isEmpty()) {
            throw new IllegalStateException("Queue is empty");
        }

        E element = elements[first];
        first = (first + 1) % capacity();
        length--;
        return element;
    }

    @Override
    public int length() {
        return length;
//        return (last - first + capacity()) % capacity();
    }


    @Override
    public Queue<E> newInstance() {
        return new CircArrayQueue<>(capacity());
    }

    @Override
    public void clear() {
        first = 0;
        last = 0;
        length = 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<>() {
            private int index = first;
            private int count = 0;

            @Override
            public boolean hasNext() {
                return count < length();
            }

            @Override
            public E next() {
                E element = elements[index];
                index = (index + 1) % capacity();
                count++;
                return element;
            }
        };
    }
}
