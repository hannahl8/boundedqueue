package boundedqueue;

import java.util.Iterator;
import java.util.List;
import java.util.LinkedList;

public class ListQueue<E> extends AbstractQueue<E> {
    private List<E> list;

    // contents == <> and capacity == 3 ==> stack = []:3
    // contents == <A> and capacity == 10 ==> stack = [A]:10
    // contents = <A, B, C> and capacity = 6 ==> queue = [A, B, C]:6

    public ListQueue(int capacity) {
        super(capacity);
        this.list = new LinkedList<>();
    }

    @Override
    public void enqueue(E element) throws IllegalStateException, IllegalArgumentException {
        if (isFull()) {
            throw new IllegalStateException("Queue is full");
        }
        if (element == null) {
            throw new IllegalArgumentException("Element cannot be null");
        }
        list.add(element);
    }

    @Override
    public E dequeue() throws IllegalStateException {
        if (isEmpty()) {
            throw new IllegalStateException("Queue is empty");
        }
        return list.remove(0);
    }

    @Override
    public int length() {
        return list.size();
    }


    @Override
    public Queue<E> newInstance() {
        return new ListQueue<>(capacity());
    }

    @Override
    public void clear() {
        list.clear();
    }

    @Override
    public Iterator<E> iterator() {
        return list.iterator();
    }

}
