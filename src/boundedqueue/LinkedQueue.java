package boundedqueue;

import java.util.Iterator;

public class LinkedQueue<E> extends AbstractQueue<E> {

    private Node<E> first;
    private Node<E> last;
    private int length;

    public LinkedQueue(int capacity) {
        super(capacity);
        this.first = null;
        this.last = null;
        this.length = 0;
    }

    @Override
    public void enqueue(E element) throws IllegalStateException, IllegalArgumentException {
        if (isFull()) {
            throw new IllegalStateException("Queue is full");
        }
        if (element == null) {
            throw new IllegalArgumentException("Element cannot be null");
        }

        Node<E> newNode = new Node<>();
        newNode.contents = element;
        newNode.next = null;

        if (isEmpty()) {
            first = newNode;
        } else {
            last.next = newNode;
        }
        last = newNode;
        length++;
    }

    @Override
    public E dequeue() throws IllegalStateException {
        if (isEmpty()) {
            throw new IllegalStateException("Queue is empty");
        }

        E element = first.contents;
        first = first.next;
        length--;
        return element;
    }

    @Override
    public int length() {
        return length;
    }

    @Override
    public Queue<E> newInstance() {
        return new LinkedQueue<>(capacity());
    }

    @Override
    public void clear() {
        first = null;
        last = null;
        length = 0;
    }

    @Override
    public Iterator<E> iterator() {
        return new Iterator<E>() {
            private Node<E> current = first;

            @Override
            public boolean hasNext() {
                return current != null;
            }

            @Override
            public E next() {
                E element = current.contents;
                current = current.next;
                return element;
            }
        };
    }

    private static class Node<E> {
        private E contents;
        private Node<E> next;

    }
}
