package boundedqueue;

import java.util.Iterator;

public abstract class AbstractQueue<E> implements Queue<E> {

    private final int capacity;

    public AbstractQueue(int capacity) {
        if (capacity <= 0) {
            throw new IllegalArgumentException("Capacity must be greater than 0");
        }
        this.capacity = capacity;
    }

    public int capacity() {
        return capacity;
    }

    @Override
    public boolean isEmpty() {
        return this.length() == 0;
    }

    @Override
    public boolean isFull() {
        return this.length() == this.capacity();
    }

    @Override
    public void appendAll(Queue<E> that) throws IllegalStateException, IllegalArgumentException {
        if (that == null) {
            throw new IllegalArgumentException("Queue cannot be null");
        }
        if (this.capacity() < this.length() + that.length()) {
            throw new IllegalStateException("Not enough capacity to append all elements from the given queue");
        }
        while (!that.isEmpty()) {
            this.enqueue(that.dequeue());
        }
    }

    @Override
    public Queue<E> copy() {
        Queue<E> copy = newInstance();
        for (E element : this) {
            copy.enqueue(element);
        }
        return copy;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        if (!(obj instanceof Queue)) {
            return false;
        }
        Queue that = (Queue) obj;
        if (this.capacity() != that.capacity()) {
            return false;
        }
        if (this.length() != that.length()) {
            return false;
        }
        Iterator<E> thisIter = this.iterator();
        Iterator<?> thatIter = that.iterator();
        while (thisIter.hasNext()) {
            E elem = thisIter.next();
            Object o = thatIter.next();
            if (!elem.equals(o)) {
                return false;
            }
        }
        return true;
    }

    @Override
    public int hashCode() {
        int result = 17;
        for (E element : this) {
            result = 31 * result + element.hashCode();
        }
        result = 31 * result + capacity();
        return result;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        for (E element : this) {
            sb.append(element);
            sb.append(", ");
        }
        // Remove the last comma and space
        if (sb.length() > 1) {
            sb.delete(sb.length() - 2, sb.length());
        }
        sb.append("]:");
        sb.append(capacity());
        return sb.toString();
    }
}
