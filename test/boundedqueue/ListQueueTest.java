package boundedqueue;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class ListQueueTest {

    private Queue<String> listQueueEmpty3;
    private Queue<String> listQueueABC6;

    private Queue<String> circArrayQueueEmpty3;
    private Queue<String> circArrayQueueABC6;

    private Queue<String> linkedQueueEmpty3;
    private Queue<String> linkedQueueABC6;

    @Before
    public void setUp() throws Exception {
        // ListQueue
        listQueueEmpty3 = new ListQueue<>(3);
        listQueueABC6 = new ListQueue<>(6);
        listQueueABC6.enqueue("A");
        listQueueABC6.enqueue("B");
        listQueueABC6.enqueue("C");

        // CircArrayQueue
        circArrayQueueEmpty3 = new CircArrayQueue<>(3);
        circArrayQueueABC6 = new CircArrayQueue<>(6);
        circArrayQueueABC6.enqueue("A");
        circArrayQueueABC6.enqueue("B");
        circArrayQueueABC6.enqueue("C");

        // LinkedQueue
        linkedQueueEmpty3 = new LinkedQueue<>(3);
        linkedQueueABC6 = new LinkedQueue<>(6);
        linkedQueueABC6.enqueue("A");
        linkedQueueABC6.enqueue("B");
        linkedQueueABC6.enqueue("C");
    }

    @Test
    public void testEmptyLength() {
        assertEquals(0, listQueueEmpty3.length());
    }

    @Test
    public void testEmptyLengthCircArray() {
        assertEquals(0, circArrayQueueEmpty3.length());
    }

    @Test
    public void testEmptyLengthLinkedQueue() {
        assertEquals(0, linkedQueueEmpty3.length());
    }


    @Test
    public void testEmptyCapacity() {
        assertEquals(3, listQueueEmpty3.capacity());
    }

    @Test
    public void testEmptyCapacityCircArray() {
        assertEquals(3, circArrayQueueEmpty3.capacity());
    }

    @Test
    public void testEmptyCapacityLinkedQueue() {
        assertEquals(3, linkedQueueEmpty3.capacity());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAbstractQueue0Capacity() {
        Queue<String> queue = new ListQueue<>(0);
    }

    @Test
    public void testEmptyEnqueueA() {
        listQueueEmpty3.enqueue("A");
        assertEquals(1, listQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueACircArray() {
        circArrayQueueEmpty3.enqueue("A");
        assertEquals(1, circArrayQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueALinkedQueue() {
        linkedQueueEmpty3.enqueue("A");
        assertEquals(1, linkedQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueADequeue() {
        listQueueEmpty3.enqueue("A");
        assertEquals(1, listQueueEmpty3.length());
        assertEquals("A", listQueueEmpty3.dequeue());
        assertEquals(0, listQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueADequeueCircArray() {
        circArrayQueueEmpty3.enqueue("A");
        assertEquals(1, circArrayQueueEmpty3.length());
        assertEquals("A", circArrayQueueEmpty3.dequeue());
        assertEquals(0, circArrayQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueADequeueLinkedQueue() {
        linkedQueueEmpty3.enqueue("A");
        assertEquals(1, linkedQueueEmpty3.length());
        assertEquals("A", linkedQueueEmpty3.dequeue());
        assertEquals(0, linkedQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueAEnqueueBDequeue() {
        listQueueEmpty3.enqueue("A");
        listQueueEmpty3.enqueue("B");
        assertEquals(2, listQueueEmpty3.length());
        assertEquals("A", listQueueEmpty3.dequeue());
        assertEquals(1, listQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueAEnqueueBDequeueCircArray() {
        circArrayQueueEmpty3.enqueue("A");
        circArrayQueueEmpty3.enqueue("B");
        assertEquals(2, circArrayQueueEmpty3.length());
        assertEquals("A", circArrayQueueEmpty3.dequeue());
        assertEquals(1, circArrayQueueEmpty3.length());
    }

    @Test
    public void testEmptyEnqueueAEnqueueBDequeueLinkedQueue() {
        linkedQueueEmpty3.enqueue("A");
        linkedQueueEmpty3.enqueue("B");
        assertEquals(2, linkedQueueEmpty3.length());
        assertEquals("A", linkedQueueEmpty3.dequeue());
        assertEquals(1, linkedQueueEmpty3.length());
    }

    @Test
    public void testABCLength() {
        assertEquals(3, listQueueABC6.length());
    }

    @Test
    public void testABCLengthCircArray() {
        assertEquals(3, circArrayQueueABC6.length());
    }

    @Test
    public void testABCLengthLinkedQueue() {
        assertEquals(3, linkedQueueABC6.length());
    }

    @Test
    public void testABCCapacity() {
        assertEquals(6, listQueueABC6.capacity());
    }

    @Test
    public void testABCCapacityCircArray() {
        assertEquals(6, circArrayQueueABC6.capacity());
    }

    @Test
    public void testABCCapacityLinkedQueue() {
        assertEquals(6, linkedQueueABC6.capacity());
    }

    @Test
    public void testABCEnqueue() {
        listQueueABC6.enqueue("D");
        assertEquals(4, listQueueABC6.length());
    }

    @Test
    public void testABCEnqueueCircArray() {
        circArrayQueueABC6.enqueue("D");
        assertEquals(4, circArrayQueueABC6.length());
    }

    @Test
    public void testABCEnqueueLinkedQueue() {
        linkedQueueABC6.enqueue("D");
        assertEquals(4, linkedQueueABC6.length());
    }

    @Test(expected = IllegalStateException.class)
    public void testABCEnqueueFull()  {
        listQueueABC6.enqueue("D");
        listQueueABC6.enqueue("E");
        listQueueABC6.enqueue("F");
        listQueueABC6.enqueue("G");
    }

    @Test(expected = IllegalStateException.class)
    public void testABCEnqueueFullCircArray()  {
        circArrayQueueABC6.enqueue("D");
        circArrayQueueABC6.enqueue("E");
        circArrayQueueABC6.enqueue("F");
        circArrayQueueABC6.enqueue("G");
    }

    @Test(expected = IllegalStateException.class)
    public void testABCEnqueueFullLinkedQueue()  {
        linkedQueueABC6.enqueue("D");
        linkedQueueABC6.enqueue("E");
        linkedQueueABC6.enqueue("F");
        linkedQueueABC6.enqueue("G");
    }

    @Test(expected = IllegalArgumentException.class)
    public void testABCEnqueueNullElement()  {
        listQueueABC6.enqueue(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testABCEnqueueNullElementCircArray()  {
        circArrayQueueABC6.enqueue(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testABCEnqueueNullElementLinkedQueue()  {
        linkedQueueABC6.enqueue(null);
    }

    @Test
    public void testABCDequeue() {
        assertEquals("A", listQueueABC6.dequeue());
        assertEquals(2, listQueueABC6.length());
    }

    @Test
    public void testABCDequeueCircArray() {
        assertEquals("A", circArrayQueueABC6.dequeue());
        assertEquals(2, circArrayQueueABC6.length());
    }

    @Test
    public void testABCDequeueLinkedQueue() {
        assertEquals("A", linkedQueueABC6.dequeue());
        assertEquals(2, linkedQueueABC6.length());
    }

    @Test(expected = IllegalStateException.class)
    public void testEmptyDequeue() {
        listQueueEmpty3.dequeue();
    }

    @Test(expected = IllegalStateException.class)
    public void testEmptyDequeueCircArray() {
        circArrayQueueEmpty3.dequeue();
    }

    @Test(expected = IllegalStateException.class)
    public void testEmptyDequeueLinkedQueue() {
        linkedQueueEmpty3.dequeue();
    }

    @Test
    public void testABCNewInstance() {
        Queue<String> queueABC6b = listQueueABC6.newInstance();
        assertEquals(6, queueABC6b.capacity());
        assertEquals(0, queueABC6b.length());
        assertTrue(queueABC6b instanceof ListQueue);
    }

    @Test
    public void testABCNewInstanceCircArray() {
        Queue<String> queueABC6b = circArrayQueueABC6.newInstance();
        assertEquals(6, queueABC6b.capacity());
        assertEquals(0, queueABC6b.length());
        assertTrue(queueABC6b instanceof CircArrayQueue);
    }

    @Test
    public void testABCNewInstanceLinkedQueue() {
        Queue<String> queueABC6b = linkedQueueABC6.newInstance();
        assertEquals(6, queueABC6b.capacity());
        assertEquals(0, queueABC6b.length());
        assertTrue(queueABC6b instanceof LinkedQueue);
    }

    @Test
    public void testEmptyNewInstance() {
        Queue<String> queueEmpty = listQueueEmpty3.newInstance();
        assertEquals(3, queueEmpty.capacity());
        assertEquals(0, queueEmpty.length());
        assertTrue(queueEmpty instanceof ListQueue);
    }

    @Test
    public void testEmptyNewInstanceCircArray() {
        Queue<String> queueEmpty = circArrayQueueEmpty3.newInstance();
        assertEquals(3, queueEmpty.capacity());
        assertEquals(0, queueEmpty.length());
        assertTrue(queueEmpty instanceof CircArrayQueue);
    }

    @Test
    public void testEmptyNewInstanceLinkedQueue() {
        Queue<String> queueEmpty = linkedQueueEmpty3.newInstance();
        assertEquals(3, queueEmpty.capacity());
        assertEquals(0, queueEmpty.length());
        assertTrue(queueEmpty instanceof LinkedQueue);
    }

    @Test
    public void testABCClear() {
        listQueueABC6.clear();
        assertEquals(0, listQueueABC6.length());
    }

    @Test
    public void testABCClearCircArray() {
        circArrayQueueABC6.clear();
        assertEquals(0, circArrayQueueABC6.length());
        assertEquals("[]:6", circArrayQueueABC6.toString());
    }

    @Test
    public void testABCClearLinkedQueue() {
        linkedQueueABC6.clear();
        assertEquals(0, linkedQueueABC6.length());
    }

    @Test
    public void testABCIterator() {
        StringBuilder result = new StringBuilder();
        for (String s : listQueueABC6) {
            result.append(s);
        }
        assertEquals("ABC", result.toString());
    }

    @Test
    public void testABCIteratorCircArray() {
        StringBuilder result = new StringBuilder();
        for (String s : circArrayQueueABC6) {
            result.append(s);
        }
        assertEquals("ABC", result.toString());
    }

    @Test
    public void testABCIteratorLinkedQueue() {
        StringBuilder result = new StringBuilder();
        for (String s : linkedQueueABC6) {
            result.append(s);
        }
        assertEquals("ABC", result.toString());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testAppendAllNull() {
        listQueueABC6.appendAll(null);
    }

    @Test(expected = IllegalStateException.class)
    public void testAppendAllFullThisLength() {
        Queue<String> queueABC3b = new ListQueue<>(3);
        queueABC3b.enqueue("D");
        queueABC3b.enqueue("E");
        queueABC3b.enqueue("F");
        queueABC3b.appendAll(listQueueABC6);
    }

    @Test
    public void testAppendAll() {
        Queue<String> queueDEF3 = new ListQueue<>(3);
        queueDEF3.enqueue("D");
        queueDEF3.enqueue("E");
        queueDEF3.enqueue("F");
        listQueueABC6.appendAll(queueDEF3);
        assertEquals(6, listQueueABC6.length());
        assertEquals(0, queueDEF3.length());
    }

    @Test
    public void testCopyABC() {
        Queue<String> queueABC6b = listQueueABC6.copy();
        assertEquals(6, queueABC6b.capacity());
        assertEquals(3, queueABC6b.length());
        assertEquals(listQueueABC6, queueABC6b);
        assertEquals("A", queueABC6b.dequeue());
        assertEquals("B", queueABC6b.dequeue());
        assertEquals("C", queueABC6b.dequeue());
    }

    @Test
    public void testCopyEmpty() {
        Queue<String> queueABC6b = listQueueEmpty3.copy();
        assertEquals(3, queueABC6b.capacity());
        assertEquals(0, queueABC6b.length());
        assertEquals(listQueueEmpty3, queueABC6b);
    }

    @Test
    public void testToString() {
        assertEquals("[A, B, C]:6", listQueueABC6.toString());
        assertEquals("[]:3", listQueueEmpty3.toString());
    }

    @Test
    public void testEqualsNull() {
        assertFalse(listQueueABC6.equals(null));
    }

    @Test
    public void testABCEqualsSelf() {
        assertTrue(listQueueABC6.equals(listQueueABC6));
    }

    @Test
    public void testABCEqualsNonQueue() {
        assertFalse(listQueueABC6.equals("[A, B, C]:6"));
    }

    @Test
    public void testABC6EqualsDifferentABC6() {
        Queue<String> queueABC6b = new ListQueue<>(6);
        queueABC6b.enqueue("A");
        queueABC6b.enqueue("B");
        queueABC6b.enqueue("C");
        assertTrue(listQueueABC6.equals(queueABC6b));
    }

    @Test
    public void testABC6EqualsDifferentABC10() {
        Queue<String> queueABC10b = new ListQueue<>(10);
        queueABC10b.enqueue("A");
        queueABC10b.enqueue("B");
        queueABC10b.enqueue("C");
        assertFalse(listQueueABC6.equals(queueABC10b));
    }

    @Test
    public void testABC6EqualsAB6() {
        Queue<String> queueAB6 = new ListQueue<>(6);
        queueAB6.enqueue("A");
        queueAB6.enqueue("B");
        assertFalse(listQueueABC6.equals(queueAB6));
    }

    @Test
    public void testEmpty3EqualsDifferebtEmpty3() {
        Queue<String> queueEmpty3b = new ListQueue<>(3);
        assertTrue(listQueueEmpty3.equals(queueEmpty3b));
    }

    @Test
    public void testEmpty3EqualsDifferebtEmpty5() {
        Queue<String> queueEmpty5 = new ListQueue<>(5);
        assertFalse(listQueueEmpty3.equals(queueEmpty5));
    }

    @Test
    public void testABC6EqualsDEF6() {
        Queue<String> queueDEF6 = new ListQueue<>(6);
        queueDEF6.enqueue("D");
        queueDEF6.enqueue("E");
        queueDEF6.enqueue("F");
        assertFalse(listQueueABC6.equals(queueDEF6));
    }

    @Test
    public void testABC6ListBasedEqualsABC6ArrayBased() {
        Queue<String> queueABC6Array = new CircArrayQueue<>(6);
        queueABC6Array.enqueue("A");
        queueABC6Array.enqueue("B");
        queueABC6Array.enqueue("C");
        assertTrue(listQueueABC6.equals(queueABC6Array));
    }

    @Test
    public void testABC6ListBasedEqualsABC6LinkedQueue() {
        Queue<String> queueABC6Linked = new LinkedQueue<>(6);
        queueABC6Linked.enqueue("A");
        queueABC6Linked.enqueue("B");
        queueABC6Linked.enqueue("C");
        assertTrue(listQueueABC6.equals(queueABC6Linked));
    }

    @Test
    public void testHashCodeABC6AndAB6() {
        Queue<String> queueAB6 = new ListQueue<>(6);
        queueAB6.enqueue("A");
        queueAB6.enqueue("B");
        assertNotEquals(listQueueABC6.hashCode(), queueAB6.hashCode());
    }

    @Test
    public void testHashCodeABC6AndDifferentABC6() {
        Queue<String> queueABC6b = new ListQueue<>(6);
        queueABC6b.enqueue("A");
        queueABC6b.enqueue("B");
        queueABC6b.enqueue("C");
        assertEquals(listQueueABC6.hashCode(), queueABC6b.hashCode());
    }

    @Test
    public void testHashCodeABC6AndABC10() {
        Queue<String> queueABC10b = new ListQueue<>(10);
        queueABC10b.enqueue("A");
        queueABC10b.enqueue("B");
        queueABC10b.enqueue("C");
        assertNotEquals(listQueueABC6.hashCode(), queueABC10b.hashCode());
    }

    @Test
    public void testABC6ListBasedHashCodeABC6ArrayBased() {
        Queue<String> queueABC6Array = new CircArrayQueue<>(6);
        queueABC6Array.enqueue("A");
        queueABC6Array.enqueue("B");
        queueABC6Array.enqueue("C");
        assertEquals(listQueueABC6.hashCode(), queueABC6Array.hashCode());
    }

    @Test
    public void testABC6ListBasedHashCodeABC6LinkedQueue() {
        Queue<String> queueABC6Linked = new LinkedQueue<>(6);
        queueABC6Linked.enqueue("A");
        queueABC6Linked.enqueue("B");
        queueABC6Linked.enqueue("C");
        assertEquals(listQueueABC6.hashCode(), queueABC6Linked.hashCode());
    }

    @Test
    public void circArrayQueueExamplePdf() {
        Queue<String> q = new CircArrayQueue<>(6);
        q.enqueue("A");
        q.enqueue("B");
        q.enqueue("C");
        q.enqueue("D");
        q.enqueue("E");
        q.dequeue();
        q.dequeue();
        q.enqueue("X");
        q.enqueue("Y");
        q.enqueue("Z");
        q.dequeue();
        q.dequeue();
        q.enqueue("W");
        q.dequeue();
        q.dequeue();
        q.enqueue("G");
        assertEquals("[Y, Z, W, G]:6", q.toString());
        assertEquals(4, q.length());
    }

}